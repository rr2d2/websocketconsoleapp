﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClientSocket
{
    class Program
    {
        private static Socket mClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static Task mConnectionTask;
        private static Task mSendTask;

        static void Main(string[] args)
        {
            Console.Title = "Client";
            LoopConnect();
            SendLoop();
            mSendTask.Wait();
            Console.ReadKey();
        }

        private static void SendLoop()
        {
            mSendTask = Task.Factory.StartNew(() =>
            {
                WaitForconnection();

                while (true)
                {
                    Console.Write("Enter A Request: ");
                    string input = Console.ReadLine();
                    byte[] buff = Encoding.ASCII.GetBytes(input);
                    mClientSocket.Send(buff);

                    byte[] receivedBuf = new byte[1024];
                    int recSize = mClientSocket.Receive(receivedBuf);
                    var msg = new byte[recSize];
                    Array.Copy(receivedBuf, msg, recSize);
                    Console.WriteLine("Recieved From Server: " + Encoding.ASCII.GetString(msg));
                }
            });
        }

        private static void WaitForconnection()
        {
            bool ft = true;

            while (!mConnectionTask.IsCompleted)
            {
                if (ft)
                {
                    Console.Write("Waiting to connect.");
                    ft = false;
                }
                Thread.Sleep(100);
                Console.Write('.');
            }
            Console.Clear();
        }

        private static void LoopConnect()
        {
            mConnectionTask = Task.Factory.StartNew(() =>
            {
                int attempts = 0;
                while (!mClientSocket.Connected)
                {
                    try
                    {
                        attempts++;
                        mClientSocket.Connect(IPAddress.Loopback, 1800);
                    }
                    catch (SocketException)
                    {
                        //Console.Clear();
                        //Console.WriteLine("Attempts to connect: " + attempts);
                    }

                }
                Console.WriteLine("Connected");
            });
        }
    }
}
