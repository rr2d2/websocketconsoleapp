﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClientSocket
{
    class MyClientSocket
    {
        private Socket mClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private Task mConnectionTask;
        private Task mSendTask;
        private IPAddress mIp;
        private int mPort;
        private byte[] mBuffer;
        private int mBuffSize;

        public MyClientSocket(IPAddress ip, int port)
        {
            mIp = ip;
            mPort = port;
            mBuffer = new byte[mBuffSize];
        }

        public void Start()
        {
            LoopConnect();
            SendLoop();
            mSendTask.Wait();
        }

        public void SendLoop()
        {
            mSendTask = Task.Factory.StartNew(() =>
            {
                WaitForconnection();

                while (true)
                {
                    Console.Write("Enter A Request: ");
                    string input = Console.ReadLine();
                    byte[] buff = Encoding.ASCII.GetBytes(input);
                    mClientSocket.Send(buff);

                    byte[] receivedBuf = new byte[1024];
                    int recSize = mClientSocket.Receive(receivedBuf);
                    var msg = new byte[recSize];
                    Array.Copy(receivedBuf, msg, recSize);
                    Console.WriteLine("Recieved From Server: " + Encoding.ASCII.GetString(msg));
                }
            });
        }

        private void WaitForconnection()
        {
            bool ft = true;

            while (!mConnectionTask.IsCompleted)
            {
                if (ft)
                {
                    Console.Write("Waiting to connect.");
                    ft = false;
                }
                Thread.Sleep(100);
                Console.Write('.');
            }
            Console.Clear();
        }

        private void LoopConnect()
        {
            mConnectionTask = Task.Factory.StartNew(() =>
            {
                int attempts = 0;
                while (!mClientSocket.Connected)
                {
                    try
                    {
                        attempts++;
                        mClientSocket.Connect(IPAddress.Loopback, 1800);
                    }
                    catch (SocketException)
                    {
                        //Console.Clear();
                        //Console.WriteLine("Attempts to connect: " + attempts);
                    }

                }
                Console.WriteLine("Connected");
            });
        }
    }
}
