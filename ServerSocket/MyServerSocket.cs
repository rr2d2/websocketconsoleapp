﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerSocket
{
    class MyServerSocket
    {
        private Socket mServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private IPAddress mIp;
        private int mPort;
        private int mBuffSize = 2048;
        private byte[] mBuffer;

        public MyServerSocket(IPAddress ip, int port)
        {
            mIp = ip;
            mPort = port;
            mBuffer = new byte[mBuffSize];
        }

        public void Start()
        {
            mServerSocket.Bind(new IPEndPoint(mIp, mPort));
            mServerSocket.Listen(1);
            mServerSocket.BeginAccept(new AsyncCallback(AcceptMessage), null);
        }

        private void AcceptMessage(IAsyncResult ar)
        {
            Socket socket;
            try
            {
                socket = mServerSocket.EndAccept(ar);
            }
            catch (ObjectDisposedException)
            {
                //Happens On exit...
                return;
            }
            socket.BeginReceive(mBuffer, 0, mBuffer.Length, SocketFlags.None, new AsyncCallback(ReceivedMessage), socket);
        }

        private void ReceivedMessage(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;
            int msgRecSize;
            try
            {
                msgRecSize = socket.EndReceive(ar);
            }
            catch (SocketException)
            {
                //IDK exc is thrown.
                socket.Close();
                return;
            }

            byte[] rawMsg = new byte[msgRecSize];
            Array.Copy(mBuffer, rawMsg, msgRecSize);

            var msg = Encoding.ASCII.GetString(rawMsg);
            Console.WriteLine("Message Recieved: " + msg);
            byte[] response = GetResponse(msg);

            socket.BeginSend(response, 0, response.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
            socket.BeginReceive(mBuffer, 0, mBuffer.Length, SocketFlags.None, new AsyncCallback(ReceivedMessage), socket);
        }

        private void SendCallback(IAsyncResult ar)
        {
            Socket sock = (Socket)ar.AsyncState;
            sock.EndSend(ar);
        }

        private byte[] GetResponse(string msg)
        {
            byte[] res;
            switch (msg.ToLower())
            {
                case "ping":
                    res = Encoding.ASCII.GetBytes("pong");
                    break;
                case "pong":
                    res = Encoding.ASCII.GetBytes("ping");
                    break;
                default:
                    res = Encoding.ASCII.GetBytes("What?");
                    break;
            }
            return res;
        }
    }
}
