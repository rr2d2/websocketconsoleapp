﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerSocket
{
    class Program
    {
        private static Socket mServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static byte[] mBuffer = new byte[2048];
        private static int mPort = 1800;

        static void Main(string[] args)
        {
            Console.Title = "Server";
            MyServerSocket myServerSocket = new MyServerSocket(IPAddress.Any, mPort);
            myServerSocket.Start();
            //SetUpServer();
            Console.ReadKey();
        }

        private static void SetUpServer()
        {
            Console.WriteLine("Setting up server...");
            mServerSocket.Bind(new IPEndPoint(IPAddress.Any, mPort));
            mServerSocket.Listen(1);
            mServerSocket.BeginAccept(new AsyncCallback(AcceptMessage), null);
        }

        private static void AcceptMessage(IAsyncResult ar)
        {
            Socket socket;

            try
            {
                socket = mServerSocket.EndAccept(ar);
            }
            catch (ObjectDisposedException)
            {
                //Happens On Exit...
                return;
            }
            socket.BeginReceive(mBuffer, 0, mBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveMessage), socket);
        }

        private static void ReceiveMessage(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;
            int bufSize;

            try
            {
                bufSize = socket.EndReceive(ar);
            }
            catch (SocketException)
            {
                //Forecful disconnect
                socket.Close();
                return;
            }

            byte[] tmpBuf = new byte[bufSize];
            Array.Copy(mBuffer, tmpBuf, bufSize);

            string msg = Encoding.ASCII.GetString(tmpBuf);
            Console.WriteLine("Txt Received: " + msg);
            byte[] msgSend = GetResponse(msg);

            socket.BeginSend(msgSend, 0, msgSend.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
            socket.BeginReceive(mBuffer, 0, mBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveMessage), socket);
        }

        private static byte[] GetResponse(string msg)
        {
            byte[] res;
            switch (msg.ToLower())
            {
                case "ping":
                    res = Encoding.ASCII.GetBytes("pong");
                    break;
                case "pong":
                    res = Encoding.ASCII.GetBytes("ping");
                    break;
                default:
                    res = Encoding.ASCII.GetBytes("What?");
                    break;
            }
            return res;
        }

        private static void SendCallback(IAsyncResult ar)
        {
            var socket = (Socket)ar.AsyncState;
            socket.EndSend(ar);
        }
    }
}
